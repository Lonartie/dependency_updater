﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DependencyUpdater
{
    class Program
    {
        static void print(string txt)
        {
            Console.WriteLine(txt);
        }

        static void println(string line)
        {
            Console.Write("\r" + line);
        }

        static void printman()
        {
            print("usage: DependencyUpdater <zip-url> <unzip-path>");
        }

        static void clear(string path)
        {
            if (Directory.Exists(path))
                Directory.Delete(path, true);
        }

        static bool download(string url)
        {
            print("downloading file: " + url);
            using (var client = new WebClient())
            {
                try
                {
                    bool download_done = false;
                    client.DownloadProgressChanged += (a, b) => { println($"download: {b.ProgressPercentage}%"); if (b.ProgressPercentage == 100) download_done = true; };
                    client.DownloadFileAsync(new Uri(url), "out.zip");

                    while (!download_done) System.Threading.Thread.Sleep(1);

                    print("\ndownload complete");
                    return true;
                }
                catch(Exception ex)
                {
                    print("\ndownload error");
                    print(ex.Message);
                    print(ex.InnerException.Message);
                    return false;
                }
            }
        }

        static void cleardownload()
        {
            if (File.Exists("out.zip"))
                File.Delete("out.zip");
        }

        static void unpack(string path)
        {
            print("unpacking...");
            call("rar x out.zip \"" + path + "\"");
        }

        static void Main(string[] args)
        {
            foreach (string arg in args)
                print("arg: \"" + arg + "\"");

            if (args.Length == 0)
            {
                printman();
                return;
            }

            string url = args[0];
            string path = args[1];

            //clear(path);
            if (!download(url)) return;
            unpack(path);
            cleardownload();
        }

        static void call(string command)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/C " + command;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.UseShellExecute = false;
            process.StartInfo = startInfo;
            process.Start();
            string output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();
            print(output);
        }
    }
}
